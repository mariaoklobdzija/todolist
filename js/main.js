// createDayDropdown();
// createMonthDropdown();
// createYearDropdown();


$(document).ready(function() {
    $('#deadLine').datepicker({
        dateFormat: "yy-mm-dd",
        minDate: '+1d'
    });
    $('.action').change(function() {
        let filter = {}

        if ($(this).val() !== '') {
            filter = {
                action: $(this).val()
            }
        }




        const ul = document.querySelector('.cont_princ_lists > ul'); //Clear data from html dom
        ul.innerHTML = '';

        $.ajax({
            url: 'https://my-json-server.typicode.com/nebojsazr/todo_service/todos',
            method: 'GET',
            data: filter,
            dataType: 'json'
        }).done(function(data) {
            if (data.length > 0) {
                for (i = 0; i < data.length; i++) {
                    console.log('element' + i, data[i]);
                    data[i].date = new Date(data[i].date);
                    createListItem(data[i]);
                }
            }
        }).fail()

        $.ajax({
            url: 'https://my-json-server.typicode.com/nebojsazr/todo_service/todos',
            method: 'GET',
            data: filter,
            dataType: 'json'
        }).done(function(data) {
            if (data.length > 0) {
                for (i = 0; i < data.length; i++) {
                    console.log('element' + i, data[i]);
                    data[i].date = new Date(data[i].date);
                    createListItem(data[i]);
                }
            }
        }).fail()
    });
});

function add_new() {

    // selektovati element  u kojem se nalazi forma

    //---------------------QUERYSELECTOR----------------------------------
    /* querySelector ()  uzima prvi element koji ima zadatu klasu koja 
    je navedena u zagradi*/
    //--------------------------------------------------------------------

    // Dodavanje ili oduzimanje css klase koja cini da forma postane vidljiva

    const forma = document.querySelector('.cont_crear_new');
    forma.classList.toggle('cont_crear_new_active');


    //---------------------TOGGLE-----------------------------------------
    /* toggle ()  Toggle between adding and removing a class name from an 
        element with JavaScript
                function myFunction() {
                var element = document.getElementById("myDIV");
                 element.classList.toggle("mystyle");}*/
    //--------------------------------------------------------------------

    //---------------------CLASSLIST--------------------------------------
    /* classList ()  - dodavanje klase 
    Get the <div> element with id="myDIV" and add the "mystyle" class to it:
            function myFunction() {
            var element = document.getElementById("myDIV");
            element.classList.add("mystyle");  } */
    //--------------------------------------------------------------------

    // Dodavanje ili oduzimanje css klase koja cini da forma postane vidljiva

}


// ===========FUNKCIJA ZA STAMPANJE ZELJENJIH PODATAKA IZ FORME=================


function add_to_list(event) {

    const deadLine = new Date(document.getElementById('deadLine').value);

    // const day = document.getElementById('dateContainer').children[0].value;
    // const month = document.getElementById('monthContainer').children[0].value;
    // const year = document.getElementById('yearContainer').children[0].value;



    // Podatke iz elemenata forme prebacujemo u promenljive da bismo ih kasnije iskoristili

    const elements = {
        action: document.getElementById('action_select'),
        title: document.querySelector('.input_title_desc'),
        date: deadLine,
        desc: document.querySelector('.input_description')

    }

    //----------------------CONST-----------------------------------------------
    /* const konstante koje se u datoj funkciji ne menjaju po jednom pozivanju
     funcije. Moze se  definisati i sa var i let ali u ovom slucaju je moguce
     da su konstante jer u toku same funkcije ne dolazi do promene vrednosti*/
    //--------------------------------------------------------------------------

    //-----action = document.getElementById('action_select').value--------------
    //              getElementsByClassName
    /* ACTION naziv konstanti koji mi dodeljujemo
       document.getElementById('action_select')- element sa datim Id-jem
       value - daj mi njegovu vrednost*/
    //--------------------------------------------------------------------------

    if (!isValid(elements)) {
        return false;
    }
    createListItem({
        action: elements.action.value,
        title: elements.title.value,
        date: elements.date,
        desc: elements.desc.value,
    });
}

function createListItem(itemData) {
    // Kreiramo node za postojeci html element u koji cemo dodavati nove elemente (planove za uraditi)

    const ul = document.querySelector('.cont_princ_lists > ul');

    /* UL je element koji se nalazi u HTML-u i u koji cemo dodavati nove elemente LI
    putem MAIN.JS
                                              querySelector('.cont_princ_lists > ul  
    CONST UL - je ul koji je direktni potomak elementa sa klasom .cont_princ_lists*/


    // Ovde cuvamo koliko child nodova (li elemenata trenutno ima u listi)
    const childNum = ul.children.length;

    //----------------------------LENGTH---------------------------------------
    // LENGTH sluzi za prebrojavanje u ovom slucaju potomaka UL elementa
    //-----------------------------------------------------------


    // Krecemo sa kreiranjem novog child noda koji se dodaje prilikom jednog izvrsavanja ove funkcije

    // Kreiramo osnovni child node element - definisanje const LI 

    //----------------------------CREATEELEMENT--------------------------------
    // CREATEELEMENT sluzi za kreiranje child node elementa
    //-------------------------------------------------------------------------

    const li = document.createElement('li');
    li.classList.add('list_shopping');

    // i dodajemo mu css klasu
    //(tu klasu mozeme u css stilizirati onako kako zelimo da se prikaze na stranici)

    // dodajemo mu jos jednu klasu koja sadrzi redni broj elementa
    // Pomocu ove klase mozemo jedinstveno identifikovati element
    const itemClass = 'li_num_0_' + (childNum + 1);
    li.classList.add(itemClass);


    // kreiranje novog DIV-a za HEADER
    const itemHeader = document.createElement('div');
    itemHeader.classList.add('item-header');
    li.appendChild(itemHeader);

    // kreira se konteiner za izracunavanje preostalog vremena

    const leftTimeContainer = document.createElement('span');
    leftTimeContainer.classList.add('left-time');
    let leftTimeContainerText = 'Time left: ';
    let leftTime = leftUntil(itemData.date);
    leftTimeContainerText += `${leftTime.months} meseci 
                                ${leftTime.days} dana
                                ${leftTime.hours} sati
                                ${leftTime.minutes} minuta
                                ${leftTime.seconds} sekundi`;

    leftTimeContainer.textContent = leftTimeContainerText;
    itemHeader.appendChild(leftTimeContainer);
    itemHeader.style.border = colorLeftTime(leftTime.days);




    //--------------------------DIV1-------------------------------------------
    // Kreiramo div koji predstavlja prvu kolonu i sadrzi podatak 'action' iz forme;
    const div1 = document.createElement('div');
    div1.className = 'col_md_1_list'; // Dodajemo mu njegovu klasu
    div1.innerHTML = `<p>${itemData.action}</p>`; // i dodajemo mu njegov sadržaj

    //----------------------------CLASSNAME------------------------------------
    // CLASSNAME    novom elementu dodajemo zaljenu klasu
    //-------------------------------------------------------------------------

    //----------------------------INNERHTML------------------------------------
    // INNERHTML    dodaje se sadrzaj tog elementa '<p>'  +   +  '<p>'
    //-------------------------------------------------------------------------



    //--------------------------DIV2-------------------------------------------
    // Kreiramo div koji predstavlja drugu kolonu i sadrzi podatke 'title' i 'description' iz forme;
    const div2 = document.createElement('div');
    div2.className = 'col_md_2_list'; // dodajemo mu njegovu klasu


    // Kreiramo pod element title kao node da bismo mogli da operisemo sa njim
    const div2Title = document.createElement('h4');
    div2Title.textContent = itemData.title;


    // POLJA KOJA MORAJU BITI POPUNJENA

    // if (title.trim() == "") {
    //     alert("TITLE must be filled out");
    //     return;
    // }
    // if (desc.trim() == "") {
    //     alert("DESCRIPTION must be filled out");
    //     return;
    // }
    /* funkcije koje ukoliko je string prazan vracaju 
    da polje mora biti popunjeno i da se funkcija ne izvrsava*/

    // Kreiramo pod element za description kao node da bismo mogli da operisemo sa njim
    const div2Desc = document.createElement('p');
    div2Desc.classList.add(`desc${childNum + 1}`); // Dodajemo mu jedinstvenu klasu koristeci redni broj
    div2Desc.textContent = itemData.desc; // i dodajemo mu text content

    // Dodajemo novokreirane podelemente u drugu kolonu
    div2.appendChild(div2Title);
    div2.appendChild(div2Desc);

    // Kreiramo event listener za click event nad elementom koji predstavlja title
    div2Title.addEventListener('click', function() {
        let desc = document.getElementsByClassName('desc' + (childNum + 1))[0]; // Selektujemo description element sa odredjenim rednim brojem
        desc.classList.toggle('hidden'); // Sakrivamo descritopn element koristeci css classu hidden
    });


    //--------------------------DIV3-------------------------------------------
    // Kreiramo div koji predstavlja trecu kolonu i sadrzi podatak 'date' iz forme;
    const div3 = document.createElement('div');
    // idodajemo mu njegovu css klasu
    div3.className = 'col_md_3_list';
    div3.innerHTML = '<div class="cont_text_date"><p>' + itemData.date.toLocaleDateString() + '</p></div>';




    // Kreiramo button elemenent koji ce se koristiti za brisanje tekuceg child elementa od liste 
    const deleteBtn = document.createElement('button');
    deleteBtn.textContent = '| DONE'; // i dodajemo mu text
    deleteBtn.className = 'button_style';

    // nad kreiranim buttonnom kreiramo event listener koji brise tekuci li element iz liste
    deleteBtn.addEventListener('click', function() {
        if (confirm('Da li ste sigurni da zelite da obrisete!')) {
            // let daLiJeSiguran = confirm('Da li ste sigurni da zelite da obrisete!')
            // if (daLiJeSiguran) {  //NEBOJSINA VARIJANTA

            ul.removeChild(li);
        }
    });

    const forma = document.querySelector('.cont_crear_new');
    forma.classList.toggle('cont_crear_new_active');
    // kada se forma potvrdi klikom na ADD ona nestaje a pojavljuje se lista



    // Dodajemo sve kreirane segmente u glavni li child node
    li.appendChild(div1);
    li.appendChild(div2);
    li.appendChild(div3);
    itemHeader.appendChild(deleteBtn);


    // I konacno dodati kreirani node u document tako da se on pojavljuje u ovom trenutku
    ul.appendChild(li);

    // Nakon sto je novi element kreiran i dodan, imalo bi smisla da se elementi forme resetuju
    // Stoga postavljamo njihove vrednosti na pocetno stanje
    document.getElementById('action_select').value = 'SHOPPING';
    document.querySelector('.input_title_desc').value = '';
    //document.getElementById('date_select').value;
    document.querySelector('.input_description').value = '';

}

function isValid(elems) {

    elems.title.classList.remove('invalid');
    elems.desc.classList.remove('invalid');

    // input pocrveni i izlazi alert za upozorenje ako je prazno polje
    let result = true;
    if (String(elems.title.value).replace(' ', '') == '') {
        elems.title.classList.add('invalid');

        //alert("TITLE must be filled out");
        result = false;
    }


    // input pocrveni i izlazi alert za upozorenje ako je prazno polje
    if (String(elems.desc.value).trim() == '') {
        elems.desc.classList.add('invalid');
        // alert("DESCRIPTION must be filled out");
        result = false;
    }

    const now = new Date();
    if (now.getTime() >= elems.date.getTime()) {
        result = false;
    }


    return result;
}

function leftUntil(deadLine) {
    let now = new Date();
    let timeLeft = new Date(deadLine - now.getTime());
    if (timeLeft.getTime() < 0) return false;

    return {
        months: timeLeft.getUTCMonth(),
        days: timeLeft.getUTCDate() - 1,
        hours: timeLeft.getUTCHours(),
        minutes: timeLeft.getUTCMinutes(),
        seconds: timeLeft.getUTCSeconds()
    }
}

function colorLeftTime(deadLine) {
    // console.log(deadLine); za proveru u konzoli da se vidi koji je rezultat
    if (deadLine >= 2) return '2px double green';
    if (deadLine >= 1) return '2px double yellow';
    if (deadLine >= 0) return '2px double red';
    return;

}